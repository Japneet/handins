#!/bin/bash
if [ $# -ne 1 ]
  then echo "Usage : There should be just one argument"
  exit 2
fi  
read -p["Enter number of child processes"]

./child.sh </dev/null &>/dev/null &
sleep 1 &
PID1=$!
sleep 2 &
PID2=$!

wait $PID1
echo 'PID1 has ended with success.'
wait
echo 'All background processes have exited.'





